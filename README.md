## Para simples utilização e uso dos scripts deste repositório

### Itens (scripts) presentes neste repositório.

1. <chatter.py> script Python para automação do projeto Chatbot.
2. <controlBot.ino> script Arduino para controle do mecanismo do robô Chatbot. 
3. <chatter_windows> e <chatter_macOS> executáveis para uso direto de acordo com o sistema operacional, não são instaláveis.
4. <README.md> e <README.txt> documentos com informações sobre uso, edição e execução dos scripts.

### Para fazer download deste projeto.

1. Acessar o [link](https://bitbucket.org/dmaraschinjr/tiki-bot/downloads/)
2. Clicar em <Download repository>

### Para clonar este repositório.

1. Abra o editor de códigos do seu uso e preferência.
2. Acesse a paleta de comandos (ou atalho).
3. <clone>
4. Indique a URL: https://dmaraschinjr@bitbucket.org/dmaraschinjr/tiki-bot.git
5. ou <git clone https://dmaraschinjr@bitbucket.org/dmaraschinjr/tiki-bot.git>
6. Selecione a pasta para salvar o projeto.

### Tecnologias e dependências necessárias para o trabalho com este projeto.
1. Script Python 
> Python 3.7+
> Editor de código (VS-Code, Notepad++, PyCharm ...)
2. Script Arduino
> Arduino IDE
3. Para trabalho com Arduino IDE online (Arduino Cloud)
> Arduino Create Agent

### Tecnologias envolvidas neste projeto.
1. Speech Recognition
2. STT => Speech To Text
3. TTS => Text To Speech
4. Web Automation
5. Serial port communication

### Para simples uso deste projeto
1. Download do executável referente ao seu sistema operacional (Windows/Mac OS) - refere-se ao script Python deste projeto.
2. Download e carregamento do script Arduino na placa.
 > Por meio da [Arduino IDE](https://www.arduino.cc/en/software)

 > Por meio da [Arduino Cloud](https://create.arduino.cc/)
 >> Necessário login e [Arduino Create Agent](https://support.arduino.cc/hc/en-us/articles/360014869820-How-to-install-the-Arduino-Create-Agent).

3. Ao executar o arquivo de <chatter.py> ou <chatter_windows> ou <chatter_macOS>, indicar a porta USB na qual o Arduino está conectado (uma lista será apresentada).
4. Digitar comandos de acordo com a sintaxe apresentada.

#### Ligações físicas para funcionamento do robô chatbot (movimento de olhos e boca)
1. 2 x servo motor (SG90); 
2. 1 Arduino Uno ou Nano;
3. Protoboard (opcional);
4. Conectar fios (jumpers) de acordo com o [esquemático](https://www.tinkercad.com/things/gNoW1eTssJW).

### Comandos compreensíveis pelo Tiki Chatbot 
* a corretude na escrita é indispensável para o correto funcionamento do projeto
* "..." deve ser substituído indicando palavra/string desejado para executar
  
1. "tiki tiki" => despertar 
2. "tiki oi" => cumprimento 
3. "tiki tchau" cumprimento
4. "tiki tocar ... " => play no youtube => ex.: "tiki tocar rock"
5. "tiki pesquisar ..." => pesquisa no google => ex.: "tiki pesquisar Africa do Sul"
6. "tiki informe ..." => retorna informação buscada na Wikipedia => ex.: "tiki informe história do Brasil"
7. "tiki abrir ..." => navega em página web => ex.: "tiki abrir facebook.com"
8. "tiki fale ..." => sintetização em voz de uma frase => ex.: "tiki fale meu mundo é um bom mundo de se viver"
9. "tiki encerrar" => finaliza a execução do chatbot