import speech_recognition as sr   # voice recognition library
import random                     # to choose random words from list
import pyttsx3                    # offline Text to Speech
import datetime                   # to get date and time
import webbrowser                 # to open and perform web tasks
import serial                     # for serial communication
import pywhatkit                  # for more web automation
import wikipedia
import sys

# Declare robot name (Wake-Up word)
robot_name = 'tiki'

# random words list
hi_words = ['oi', 'olá', 'hello', 'hallo']
bye_words = ['tchau', 'até logo', 'bye bye', 'bye', 'totsiens']

# initilize things
engine = pyttsx3.init()                    # init text to speech engine
engine.setProperty("rate", 180)            # speech speed
voices = engine.getProperty("voices")

for voice in voices:
    #print(voice.id)
    if "PT-BR" in voice.id:
        engine.setProperty('voice', voice.id)
        break

wikipedia.set_lang("pt")     # bot info em portugues
#listener = sr.Recognizer()   # initialize speech recognition API

class SerialMock:
    def write(self, txt):
        pass


# connect with NiNi motor driver board over serial communication
try:
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    ports = sorted(ports)
    i = 0
    for port, desc, hwid in ports:
        print("[{}]: {}".format(i, port))
        i = i+1
    serial_port = ports[int(input("Selecione uma porta USB: "))]
    port = serial.Serial(serial_port.name, 9600)
    print("Phycial body, connected.")

except Exception as e:
    print("Unable to connect to my physical body.")
    print(e)

    port = SerialMock()


def listen():
    """ listen to what user says"""
    try:
        #with sr.Microphone() as source:                         # get input from mic
        #    print("Talk>>")
        #    # listen from microphone
        #    voice = listener.listen(source)
        #    command = listener.recognize_google(
        #        voice, language="pt-BR").lower()  # use google API
        #    # all words lowercase- so that we can process easily
        #    #command = command.lower()
        #    print(command)

        # look for wake up word in the beginning
        command = input("Aguardando comando: ").lower()

        if (command.split(' ')[0] == robot_name):
            # if wake up word found....
            print("Chamado para despertar recebido!")
            port.write(b's')
            # call process funtion to take action
            process(command)

    except Exception as e:
        port.write(b'u')
        print("Comando não compreendido.")
        talk("Tikí não pode ajudar!")
        print(e)


def process(words):
    """ process what user says and take actions """
    print(words)  # check if it received any command

    # break words in
    # split by space and ignore the wake-up word
    word_list = words.split(' ')[1:]

    if (len(word_list) == 1):
        if (word_list[0] == robot_name):
            port.write(b'c')
            talk("como posso ajudar?")
            # .write(b'l')
            return

    if word_list[0] == 'tocar':
        """if command for playing things, play from youtube"""
        port.write(b'p')
        talk("Ok, abrindo youtube.")
        # search without the command word
        extension = ' '.join(word_list[1:])
        pywhatkit.playonyt(extension)
        return

    elif word_list[0] == 'pesquisar':
        """if command for google search"""
        port.write(b'g')
        talk("Ok, pesquisando "+' '.join(word_list[1:]))
        extension = ' '.join(word_list[1:])
        pywhatkit.search(extension)
        return

    elif word_list[0] == 'informe':
        """if command for getting info"""
        # search without the command words
        port.write(b'u')
        talk("Ok mestre! Vou informar.")
        extension = ' '.join(word_list[1:])
        inf = pywhatkit.info(extension, lines=2, return_value=True)
        # read from result
        print(inf)
        port.write(b'i')
        talk(inf)
        return

    elif word_list[0] == 'abrir':
        """if command for opening URLs"""
        #port.write(b'l')
        if(word_list[1]==""):
            port.write(b'u')
            talk("Tiki não entendeu!")
        else:
            port.write(b'u')
            talk("Entendido! Navegando...")
            url = f"http://{''.join(word_list[1:])}"   # make the URL
            webbrowser.open(url)
            return

    elif word_list[0] == 'fale':
        """if command for voice-synthesize a string"""
        print('Sintetizando...')
        talk(str(' '.join(word_list[1:])))
        return

    elif word_list[0] == 'encerrar':
        """if command for finish execution"""
        port.write(b'u')
        talk("Foi um prazer caro supremo!")
        port.write(b'h')
        talk(random.choice(bye_words))
        sys.exit()

    # now check for matches
    for word in word_list:
        if word in hi_words:
            """ if user says hi/hello greet him accordingly"""
            port.write(b'h')            
            talk(random.choice(hi_words))

        elif word in bye_words:
            """ if user says bye etc"""
            port.write(b'h') 
            talk(random.choice(bye_words))

def talk(sentence):
    """ talk / respond to the user """
    engine.say(sentence)
    engine.runAndWait()

# run the app
while True:
    listen()  # runs listen one time