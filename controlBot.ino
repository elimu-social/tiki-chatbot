#include <Servo.h>

Servo boca;
Servo olhos;

/*
 * olhos max para cima 0
 * olhos max para baixo 60
 * olhos ao meio 50
 * boca max aberto 0
 * boca max fechado 90
 */

byte val = ""; //recebe valores da porta serial

void setup() {
  boca.attach(5);
  olhos.attach(6);
  Serial.begin(9600);
}

//sleep position
void standby() {
  // posicao de repouso
  boca.write(90);
  olhos.write(40);
}

//command not understood
void not_understood() {
  int randNumb;
  for(int i=0; i<=5; i++){
    //movimentos aleatorios de boca
    boca.write(random(20, 90));
    delay(300);
  }
  delay(300);
  standby();
}

//command: ''tiki tiki'' (wake up)
void call() {
  olhos.write(40);
  for(int i=0; i<3; i++){
    //abre boca
    for(int j=90; j>=0; j--){
      boca.write(j);
      delay(3);
    }
    delay(10);
    //fecha boca
    for(int j=0; j<=90; j++){
      boca.write(j);
      delay(3);
    }
  }
  delay(300);
  standby();
}

//command: ''tiki hi''
void hi() {
  int i=0;
  //levanta olhos
  for(i=60; i>=0; i--){
    olhos.write(i);
    delay(3);
  }
  //abre boca
  for(i=90; i>=0; i--){
    boca.write(i);
    delay(5);
  }
  //fecha boca
  for(i=0; i<=90; i++){
    boca.write(i);
    delay(5);
  }
  //abaixa olhos
  for(i=0; i<=60; i++){
    olhos.write(i);
    delay(5);
  }

  delay(300);
  standby();
}

//command: ''tiki tocar ...''
void play() {
  for(int i=0; i<4; i++){
    //abre boca
    for(int j=90; j>=0; j--){
      boca.write(j);
      delay(3);
    }
    delay(10);
    //fecha boca
    for(int j=0; j<=90; j++){
      boca.write(j);
      delay(3);
    }
  }
  /*
  delay(15000); //~tempo para que a musica inicie
  olhos.write(50);
  
  //movimentos aleatorios de boca
  int randNumb;
  for(int i=0; i<=200; i++){
    boca.write(random(20, 90));
    delay(150);
  }
  */
  delay(300);
  standby();
}

//command: ''tiki pesquisar ...''
void search(){
  for(int i=0; i<5; i++){
    //abre boca
    for(int j=90; j>=0; j--){
      boca.write(j);
      delay(5);
    }
    delay(10);
    //fecha boca
    for(int j=0; j<=90; j++){
      boca.write(j);
      delay(3);
    }
  }
  delay(300);
  standby();
}

//command: ''tiki informe ...''
void info(){
  //movimentos aleatorios de boca
  /*
  int randNumb;
  for(int i=0; i<=20; i++){
    boca.write(random(20, 90));
    delay(150);
  }
  */
  delay(5000); 
  for(int i=0; i<=150; i++){
    boca.write(random(0, 90));
    olhos.write(random(30, 60));
    delay(200);
  }
  
  delay(300);
  standby();
}

/*
 * 
*/
void loop() {
  while (Serial.available() > 0){ //look for serial data available or not
    val = Serial.read();          //read the serial value

    if (val == 'c') {
      // do call
      call();
    }
    if (val == 'h') {
      // do hi
      hi();
    }
    if (val == 'p') {
      // do play on youtube
      play();
    }
    if (val == 'g') {
      // do search on google
      search();
    }
    if (val == 'i') {
      // do get info on wiki
      info();
    }
    
    if (val == 'u') {
      // command not understood
      not_understood();
    }
    if (val == 's') {
      // do standby
      standby();
    }
  }
  
}